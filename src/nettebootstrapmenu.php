<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        'NetteBootstapMenu\\RootBranchHaveParentException' => 'exceptions.php',
        'NetteBootstapMenu\\Menu\\IMenu' => 'Menu/IMenu.php',
        'NetteBootstapMenu\\Menu\\IMenuItem' => 'Menu/IMenuItem.php',
        'NetteBootstapMenu\\Menu\\Menu' => 'Menu/Menu.php',
        'NetteBootstapMenu\\Menu\\MenuItem' => 'Menu/MenuItem.php',
        'NetteBootstapMenu\\Menu\\MenuSeparator' => 'Menu/MenuSeparator.php',
        'NetteBootstapMenu\\Menu\\MenuSearch' => 'Menu/MenuSearch.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/NetteBootstapMenu/' . $classMap[$className];
    }
});
