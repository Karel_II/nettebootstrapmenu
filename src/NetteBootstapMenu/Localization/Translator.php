<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Localization;

/**
 * Description of Translator
 *
 * @author karel.novak
 */
class Translator implements \Nette\Localization\ITranslator {

    /**
     * Translates the given string
     *
     * @param string $message
     * @param integer $count plural count
     * @return string
     */
    function translate($message, ...$parameters): string {
        $arguments = func_get_args();
        return vsprintf(array_shift($arguments), $arguments);
    }
}
