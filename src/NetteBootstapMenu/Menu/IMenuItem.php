<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 *
 * @author Karel
 */
interface IMenuItem {
    /* Index */

    /**
     * 
     * @return integer
     */
    public function getId();

    /**
     * 
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /* Texts */

    /**
     * 
     * @return string|null
     */
    public function getName();

    /**
     * 
     * @return string|null
     */
    public function getDescription();

    /**
     * 
     * @return string|null
     */
    public function getIcon();

    /**
     * 
     * @return string|null
     */
    public function getClass();

    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * 
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * 
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon);

    /**
     * 
     * @param string $class
     * @return $this
     */
    public function setClass($class);

    /* Menu Helpers */

    /**
     * 
     * @return bool
     */
    public function getShowMenu();

    /**
     * 
     * @return bool
     */
    public function isContainSubMenu();

    /**
     * 
     * @param bool $showMenu
     * @return $this
     */
    public function setShowMenu($showMenu);

    /* Tree Helpers */

    /**
     * Return level of menu item
     * @return int
     */
    public function getLevel();

    /**
     * Return path of menu item
     * @param string $glue
     * @return string
     */
    public function joinParentsNames($glue);

    /* Magic Methods */

    public function __toString();

    public function __clone();
}
