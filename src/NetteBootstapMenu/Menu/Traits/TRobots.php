<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

use NetteBootstapMenu\Menu\Interfaces\IRobots,
    NetteBootstapMenu\Menu\Interfaces\IMenuHasParent;
use Nette\Utils\Validators;

/**
 *
 * @author karel.novak
 * @property string|null $aRel Description
 * @property string|null $metaRobots Description
 * @property boolean $disallow Description
 */
trait TRobots {

    /**
     *
     * @var array 
     */
    protected $aRel = array();

    /**
     *
     * @var array 
     */
    protected $metaRobots = array();

    /**
     * 
     * @return string|null
     */
    public function getARel() {
        if (!empty($this->aRel)) {
            return implode(' ', $this->aRel);
        }
        if ($this instanceof IMenuHasParent && isset($this->parent) && $this->parent instanceof IRobots && (($aRel = $this->parent->getARel()) !== NULL)) {
            return $aRel;
        }
        return NULL;
    }

    /**
     * 
     * @return string|null
     */
    public function getMetaRobots() {
        if (!empty($this->metaRobots)) {
            return implode(', ', $this->metaRobots);
        }
        if ($this instanceof IMenuHasParent && isset($this->parent) && $this->parent instanceof IRobots && (($metaRobots = $this->parent->getMetaRobots()) !== NULL)) {
            return $metaRobots;
        }
        return NULL;
    }

    /**
     * 
     * @return boolean
     */
    public function isDisallow() {
        if (
                (!empty($this->aRel) && in_array(IRobots::A_REL_NOFOLLOW, $this->aRel)) ||
                (!empty($this->metaRobots) && (in_array(IRobots::META_ROBOTS_NOINDEX, $this->metaRobots) || in_array(IRobots::META_ROBOTS_NONE, $this->metaRobots)))) {
            return TRUE;
        }
        if ($this instanceof IMenuHasParent && isset($this->parent) && $this->parent instanceof IRobots && (($disallow = $this->parent->isDisallow()) !== FALSE)) {
            return $disallow;
        }
        return FALSE;
    }

    /**
     * 
     * @param string $aRel
     * @return $this
     * @throws Exception
     */
    public function addARel($aRel) {
        if (preg_match('/^(' . IRobots::A_REL_ALTERNATE
                        . '|' . IRobots::A_REL_AUTHOR
                        . '|' . IRobots::A_REL_BOOKMARK
                        . '|' . IRobots::A_REL_EXTERNAL
                        . '|' . IRobots::A_REL_HELP
                        . '|' . IRobots::A_REL_LICENSE
                        . '|' . IRobots::A_REL_NEXT
                        . '|' . IRobots::A_REL_NOFOLLOW
                        . '|' . IRobots::A_REL_NOREFERRER
                        . '|' . IRobots::A_REL_NOOPENER
                        . '|' . IRobots::A_REL_PREV
                        . '|' . IRobots::A_REL_SEARCH
                        . '|' . IRobots::A_REL_TAG
                        . ')$/', $aRel)) {
            if (in_array($aRel, $this->aRel)) {
                return $this;
            }
            $this->aRel[] = $aRel;
            return $this;
        }
        throw new \Exception('Wrong rel value');
    }

    /**
     * 
     * @param string $metaRobots
     * @return $this
     * @throws Exception
     */
    public function addMetaRobots($metaRobots) {
        if (preg_match('/^(' . IRobots::META_ROBOTS_ALL
                        . '|' . IRobots::META_ROBOTS_NOARCHIVE
                        . '|' . IRobots::META_ROBOTS_NOFOLOW
                        . '|' . IRobots::META_ROBOTS_NOIMAGEINDEX
                        . '|' . IRobots::META_ROBOTS_NOINDEX
                        . '|' . IRobots::META_ROBOTS_NONE
                        . '|' . IRobots::META_ROBOTS_NOSNIPPET
                        . '|' . IRobots::META_ROBOTS_UNAVAILABLE_AFTER
                        . ')$/', $metaRobots)) {
            if (in_array($metaRobots, $this->metaRobots)) {
                return $this;
            }
            $this->metaRobots[] = $metaRobots;
            return $this;
        }
        throw new \Exception('Wrong metaRobots value');
    }

    /**
     * 
     * @param string|array $aRel
     * @return $this
     * @throws Exception
     */
    public function setARel($aRel) {
        if (Validators::isList($aRel)) {
            foreach ($aRel as $item) {
                $this->addARel($item);
            }
        } elseif (Validators::is($aRel, 'string:1..')) {
            foreach (explode(' ', $aRel) as $item) {
                $this->addARel($item);
            }
        } else {
            throw new \Exception('Wrong rel value');
        }
        return $this;
    }

    /**
     * 
     * @param string|array $metaRobots
     * @return $this
     * @throws Exception
     */
    public function setMetaRobots($metaRobots) {
        if (Validators::isList($metaRobots)) {
            foreach ($metaRobots as $item) {
                $this->addMetaRobots($item);
            }
        } elseif (Validators::is($metaRobots, 'string:1..')) {
            foreach (explode(', ', $metaRobots) as $item) {
                $this->addMetaRobots($item);
            }
        } else {
            throw new \Exception('Wrong metaRobots value');
        }
        return $this;
    }

}
