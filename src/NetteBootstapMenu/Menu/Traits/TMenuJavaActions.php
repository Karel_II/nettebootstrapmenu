<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
 *
 * @author Karel
 */
trait TMenuJavaActions {

    private $dataset = array();
    private $events = array();

    public function getDataset() {
        return $this->dataset;
    }

    public function getEvents() {
        return $this->events;
    }

    public function getJavaActionString() {
        $return = '';
        foreach ($this->dataset as $key => $value) {
            $return .= ('data-' . $key . '="' . $value . '" ');
        }
        foreach ($this->events as $key => $value) {
            $return .= ('on' . ucfirst($key) . '="' . $value . '" ');
        }
        return $return;
    }

    public function setJavaActions($dataset, $events) {
        $this->dataset = $dataset;
        $this->events = $events;
        return $this;
    }

}
