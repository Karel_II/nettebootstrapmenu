<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
  <loc>	        Ano	Obsahuje úplnou URL stránky, včetně protokolu (např. http, https) a ukončený lomítkem, pokud to server vyžaduje. Hodnota musí být kratší než 2048 znaků.
  <lastmod>	Ne	Datum poslední modifikace stránky v ISO 8601 formátu. Může obsahovat jednak plný datum a čas nebo může být ve formátu: RRRR-MM-DD.
  <changefreq>	Ne	Určuje jak často je stránka měněna:
  always - vždy
  hourly - každou hodinu
  daily - každý den
  weekly - týdně
  monthly - měsíčně
  yearly - ročně
  never - nikdy
  <priority>	Ne	Určuje prioritu stránky na serveru. Povolené hodnoty jsou od 0.0 do 1.0, kde 1.0 značí nejdůležitější. Defaultní hodnota je 0.5.
 * 
 * 
 * 
 * @author karel.novak
 * 
 * @property-read string $siteMapLastmod Description
 * @property-read string $siteMapChangefreq Description
 * @property-read string $siteMapPriority Description
 * @property-read boolean $siteMap Description  
 */
trait TMenuSiteMap {

    protected $siteMapLastmod = NULL;
    protected $siteMapChangefreq = NULL;
    protected $siteMapPriority = NULL;
    protected $siteMap = FALSE;

    public function getSiteMapLastmod() {
        return $this->siteMapLastmod;
    }

    public function getSiteMapChangefreq() {
        return $this->siteMapChangefreq;
    }

    public function getSiteMapPriority() {
        return $this->siteMapPriority;
    }

    public function getSiteMap() {
        return $this->siteMap;
    }

    public function setSiteMap($siteMapPriority = NULL, $siteMapChangefreq = NULL, $siteMapLastmod = NULL) {
        $this->siteMapPriority = $siteMapPriority;
        $this->siteMapChangefreq = $siteMapChangefreq;
        $this->siteMapLastmod = $siteMapLastmod;
        $this->siteMap = TRUE;
        return $this;
    }

}
