<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
 *
 * @author karel.novak
 * 
 * @property int $metaRefresh Description
 */
trait TMenuMetaRefresh {

    protected $metaRefresh = NULL;

    /**
     * 
     * @return int
     */
    public function getMetaRefresh() {
        return $this->metaRefresh;
    }

    /**
     * 
     * @param int $metaRefresh
     * @return $this
     */
    public function setMetaRefresh($metaRefresh) {
        $this->metaRefresh = (is_int($metaRefresh) || is_string($metaRefresh) && preg_match('#^[0-9]+\z#', $metaRefresh)) ? (integer) $metaRefresh : NULL;
        return $this;
    }

}
