<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
 *
 * @author Karel
 * 
 * @property string $badgeText Description
 */
trait TMenuBadge {

    protected $badgeText = NULL;

    /**
     * 
     * @return string|null
     */
    public function getBadgeText() {
        return $this->badgeText;
    }

    /**
     * 
     * @param string $badgeText
     * @return $this
     */
    public function setBadgeText($badgeText) {
        $this->badgeText = (is_string($badgeText) && !empty($badgeText)) ? $badgeText : NULL;
        return $this;
    }

}
