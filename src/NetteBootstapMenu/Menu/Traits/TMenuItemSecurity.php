<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

use \Nette\Security\IAuthorizator,
    \Nette\Security\User;

/**
 *
 * @author Karel
 * 
 * 
 * 
 */
trait TMenuItemSecurity {

    protected $securityLoggedIn = NULL;
    protected $securityResource = TRUE;
    protected $securityPrivilege = NULL;

    public function isAllowed($resource = IAuthorizator::ALLOW, $privilege = IAuthorizator::ALL) {
        $this->securityPrivilege = $privilege;
        $this->securityResource = $resource;
        return $this;
    }

    public function isLoggedIn($loggedIn = NULL) {
        $this->securityLoggedIn = $loggedIn;
        return $this;
    }

    public function checkSecurity(User $user) {
        $return = TRUE;
        if (empty($this->parent)) {
            $return = $this->parent->checkSecurity($user);
        }
        return $return && (($this->securityLoggedIn === NULL) || ($this->securityLoggedIn === $user->loggedIn)) && (($this->securityResource === IAuthorizator::ALLOW) || ($user->isAllowed($this->securityResource, $this->securityPrivilege)));
    }

}
