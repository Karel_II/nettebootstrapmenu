<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
 *
 * @author Karel
 * 
 * @property Url $url Description
 */
trait TMenuUrl {

    /**
     *
     * @var \Nette\Http\Url 
     */
    protected $url = NULL;

    /**
     * 
     * @return \Nette\Http\Url
     */
    function getUrl() {
        return $this->url;
    }

    /**
     * 
     * @param \Nette\Http\Url $url
     */
    function setUrl(\Nette\Http\Url $url) {
        $this->url = $url;
    }

}
