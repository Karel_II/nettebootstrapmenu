<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

/**
 *
 * @author karel.novak
 * @property bool $checked Description
 */
trait TMenuCheck {

    /**
     *
     * @var bool 
     */
    protected $checked = FALSE;

    /**
     * 
     * @return bool
     */
    public function getChecked() {
        return $this->checked;
    }

    /**
     * 
     * @param bool $checked
     * @return $this
     */
    public function setChecked($checked) {
        $this->checked = ($checked === TRUE) ? $checked : FALSE;
        return $this;
    }

}
