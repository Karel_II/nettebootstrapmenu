<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

use NetteBootstapMenu\Menu\Interfaces,
    NetteBootstapMenu\Menu\IMenuItem,
    NetteBootstapMenu\Menu\Menu;

/**
 * Description of TMenuHasBranches
 *
 * @author karel.novak
 * 
 * @property-read Menu $branches Description
 * @property-read Menu $subTree Description 
 */
trait TMenuHasBranches {

    protected $branches = null;

    public function getBranches() {
        if (empty($this->branches)) {
            return new Menu();
        }
        return $this->branches;
    }

    public function getSubTree(IMenuItem $parent = null) {
        $menu = new Menu();
        $menu[] = $clone = clone $this;
        if (isset($parent)) {
            $parent->addBranch($clone);
        }
        if (empty($this->branches)) {
            return $menu;
        }
        foreach ($this->branches as $branch) {
            $subBranches = $branch->getSubTree($this);
            foreach ($subBranches as $subBranch) {
                $menu[] = $subBranch;
            }
        }
        return $menu;
    }

    public function addBranch(IMenuItem $branch) {
        if (!($branch instanceof Interfaces\IMenuHasParent)) {
            throw new \NetteBootstapMenu\RootBranchHaveParentException();
        }
        if (empty($this->branches)) {
            $this->branches = new Menu();
        }
        $this->branches[] = $branch;
        $branch->setParent($this);
    }

}
