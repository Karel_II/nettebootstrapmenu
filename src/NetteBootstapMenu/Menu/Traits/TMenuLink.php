<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

use Nette\Application\UI\Presenter;

/**
 *
 * @author karel.novak
 * 
 * @property int $linkDestination Description
 * @property array $linkArgs Description
 */
trait TMenuLink {

    protected $linkDestination = NULL;
    protected $linkArgs = NULL;

    /* Link */

    public function getLinkDestination() {
        return $this->linkDestination;
    }

    public function getLinkArgs() {
        if (empty($this->linkArgs)) {
            return array();
        }
        return $this->linkArgs;
    }

    public function setLinkDestination($linkDestination) {
        $this->linkDestination = $linkDestination;
    }

    public function setLinkArgs($linkArgs) {
        $this->linkArgs = $linkArgs;
    }

    public function comparePresenter(Presenter $presenter, $parameters = NULL) {
        if (($this->linkDestination !== NULL) && preg_match("#[:]?$presenter->name[:]$presenter->action#i", $this->linkDestination) && ($this->linkArgs === NULL || $parameters === NULL || $this->linkArgs === array_intersect_assoc($parameters, $this->linkArgs))) {
            return true;
        }
    }
}
