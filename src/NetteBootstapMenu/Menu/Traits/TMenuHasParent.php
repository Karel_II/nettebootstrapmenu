<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Traits;

use NetteBootstapMenu\Menu\Interfaces,
    NetteBootstapMenu\Menu\IMenuItem,
    NetteBootstapMenu\Menu\Menu;

/**
 *
 * @author karel.novak
 * 
 * @property int $parentId Description
 * @property MenuItem $parent Description
 * @property-read Menu $parents Description 
 */
trait TMenuHasParent {

    protected $parentId = NULL;
    protected $parent = null;

    public function getParentId() {
        return $this->parentId;
    }

    public function getParent() {
        return $this->parent;
    }

    public function getParents() {
        if (isset($this->parent) && $this->parent instanceof Interfaces\IMenuHasParent) {
            $parents = $this->parent->getParents();
            if (!isset($parents)) {
                $parents = new Menu();
            }
            $parents[] = $this->parent;
            return $parents;
        }
        return null;
    }

    public function setParentId($parentId) {
        $this->parentId = (is_int($parentId) || is_string($parentId) && preg_match('#^[0-9]+\z#', $parentId)) ? (integer) $parentId : NULL;
        if (!($this instanceof Interfaces\IMenuHasParent)) {
            throw new \NetteBootstapMenu\RootBranchHaveParentException();
        }
    }

    public function setParent(IMenuItem $parent) {
        $this->parent = (empty($parent)) ? NULL : $parent;
        if (!($this instanceof Interfaces\IMenuHasParent)) {
            throw new \NetteBootstapMenu\RootBranchHaveParentException();
        }
    }

}
