<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuItem
 *
 * @author Karel_II
 * @property int $id Description
 * @property string|null $name Description
 * @property string|null $description Description
 * @property string|null $icon Description
 * @property string|null $class Description
 * 
 * @property bool $showMenu Description 
 * @property-read bool $containSubMenu Description
 *  
 * @property-read int $level Description

 */
abstract class MenuObject implements IMenuItem {

    use \Nette\SmartObject;

    /**
     *
     * @var integer 
     */
    protected $id = NULL;

    /**
     * @var string|null  
     */
    protected $name = NULL;

    /**
     * @var string|null 
     */
    protected $description = NULL;

    /**
     * @var string|null  
     */
    protected $icon = NULL;

    /**
     * @var string|null  
     */
    protected $class = NULL;

    /**
     * @var bool  
     */
    protected $showMenu = TRUE;

    /* Index */

    /**
     * 
     * @return integer
     */
    final public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param integer $id
     * @return $this
     */
    final public function setId($id) {
        $this->id = (is_int($id) || is_string($id) && preg_match('#^[0-9]+\z#', $id)) ? (integer) $id : NULL;
        return $this;
    }

    /* Texts */

    /**
     * 
     * @return string|null
     */
    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @return string|null
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * 
     * @return string|null
     */
    public function getIcon() {
        return $this->icon;
    }

    /**
     * 
     * @return string|null
     */
    function getClass() {
        return $this->class;
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = (is_string($name) && !empty($name)) ? $name : NULL;
        return $this;
    }

    /**
     * 
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = (is_string($description) && !empty($description)) ? $description : NULL;
        return $this;
    }

    /**
     * 
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon) {
        $this->icon = (is_string($icon) && !empty($icon)) ? $icon : NULL;
        return $this;
    }

    /**
     * 
     * @param string $class
     * @return $this
     */
    function setClass($class) {
        $this->class = (is_string($class) && !empty($class)) ? $class : NULL;
        return $this;
    }

    /* Menu Helpers */

    /**
     * 
     * @return bool
     */
    public function getShowMenu() {
        if (($this instanceof Interfaces\IMenuHasParent) && isset($this->parent)) {
            return $this->showMenu && ((isset($this->parent)) ? $this->parent->showMenu : TRUE);
        }
        return $this->showMenu;
    }

    /**
     * 
     * @return bool
     */
    public function isContainSubMenu() {
        if (($this instanceof Interfaces\IMenuHasBranches) && isset($this->branches)) {
            foreach ($this->branches as $branch) {
                if ($branch->showMenu) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    /**
     * 
     * @param bool $showMenu
     * @return $this
     */
    public function setShowMenu($showMenu) {
        $this->showMenu = ($showMenu === TRUE) ? $showMenu : FALSE;
        return $this;
    }

    /**
     * Return level of menu item
     * @return int
     */
    public function getLevel() {
        if (($this instanceof Interfaces\IMenuHasParent) && isset($this->parent)) {
            return $this->parent->level + 1;
        }
        return 1;
    }

    /**
     * Return path of menu item
     * @param string $glue
     * @return string
     */
    public function joinParentsNames($glue = '/') {
        if (($this instanceof Interfaces\IMenuHasParent) && isset($this->parent) && ($this->parent instanceof Interfaces\IMenuHasParent)) {
            return $this->parent->joinParentsNames($glue) . $glue . $this->name;
        } else {
            return $this->name;
        }
    }

    public function __toString() {
        return (string) $this->name;
    }

    public function __clone() {
        $this->branches = NULL;
        $this->parent = NULL;
    }

}
