<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuCheckBox
 *
 * @author Karel
 */
class MenuCheckBox extends MenuObject implements Interfaces\IMenuItemSecurity, Interfaces\IMenuLink, Interfaces\IMenuHasParent, Interfaces\IMenuCheck {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuLink;
    use Traits\TMenuHasParent;
    use Traits\TMenuCheck;

    /* Static Creators */

    public static function newMenuCheckBox($id, $parentId, $name, $description, $icon, $linkDestination, $linkArgs = array(), $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setLinkDestination($linkDestination);
        $return->setLinkArgs($linkArgs);
        $return->setParentId($parentId);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
