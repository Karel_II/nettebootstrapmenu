<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuUrl
 *
 * @author Karel
 */
class MenuUrl extends MenuObject implements Interfaces\IMenuItemSecurity, Interfaces\IMenuUrl, Interfaces\IMenuHasParent, Interfaces\IRobots {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuHasParent;
    use Traits\TMenuUrl;
    use Traits\TRobots;


    /* Static Creators */

    public static function newMenuUrl($id, $parentId, $name, $description, $icon, \Nette\Http\Url $url, $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setUrl($url);
        $return->setParentId($parentId);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
