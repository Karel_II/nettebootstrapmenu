<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

use Nette,
    Nette\Utils\ArrayHash,
    Nette\Application\UI\Presenter;

/**
 * Description of Menu
 *
 * @author Karel_II
 */
class Menu implements IMenu {

    /**
     *
     * @var string 
     */
    protected $dataType = '\NetteBootstapMenu\Menu\IMenuItem';

    /**
     *
     * @var  MenuItem
     */
    protected $data = null;

    public function __construct() {
        $this->data = new ArrayHash();
    }

    private static function extractPresenterParameters(Presenter $presenter) {
        $parameters = $presenter->request->parameters;
        unset($parameters['request']);
        return array_diff($parameters, array("action" => $presenter->action, "NULL" => NULL));
    }

    /**
     * 
     * @param Presenter $presenter
     * @return MenuItem|NULL
     */
    public function getMenuItem(Presenter $presenter) {
        $parameters = Menu::extractPresenterParameters($presenter);
        foreach ($this->data as $menuItem) {
            if ($menuItem instanceof Interfaces\IMenuLink && $menuItem->comparePresenter($presenter, $parameters)) {
                return $menuItem;
            }
        }

        foreach ($this->data as $menuItem) {
            if ($menuItem instanceof Interfaces\IMenuLink && $menuItem->comparePresenter($presenter, NULL)) {
                return $menuItem;
            }
        }
        return NULL;
    }

    public function offsetSet($key, $value): void {

        if (empty($key)) {
            $key = (int) $value->id;
        }

        if ($value instanceof Interfaces\IMenuHasParent && $value->parentId !== NULL && isset($this->data[$value->parentId])) {
            $this->data[$value->parentId]->addBranch($value);
        }

        if (!is_a($value, $this->dataType)) {
            throw new Nette\InvalidArgumentException(sprintf('Value must be either a ' . $this->dataType . ', %s given.', get_class($value)));
        }

        if (!is_scalar($key)) { // prevents NULL
            throw new Nette\InvalidArgumentException(sprintf('Key must be either a string or an integer, %s given.', gettype($key)));
        }
        $this->data->$key = $value;
    }

    /**
     * 
     * @param \NetteBootstapMenu\Menu\IMenu $menu
     * @throws \Nette\InvalidArgumentException
     */
    public function merge(IMenu $menu) {
        foreach ($menu as $key => $value) {
            $this->data->$key = $value;
        }
    }

    /**
     * Returns an iterator over all items.
     * @return \RecursiveArrayIterator
     */
    public function getIterator(): \Traversable {
        return new \RecursiveArrayIterator($this->data);
    }

    /**
     * Returns items count.
     * @return int
     */
    public function count(): int {
        return count((array) $this->data);
    }

    /**
     * Returns a item.
     * @return mixed
     */
    public function offsetGet($key): mixed {
        return $this->data->$key;
    }

    /**
     * Determines whether a item exists.
     * @return bool
     */
    public function offsetExists($key): bool {
        return isset($this->data->$key);
    }

    /**
     * Removes the element from this list.
     * @return void
     */
    public function offsetUnset($key): void {
        unset($this->data->$key);
    }

    /**
     * Return keys of all elements
     * @return array
     */
    public function getKeys() {
        return array_keys((array) $this->data);
    }
}
