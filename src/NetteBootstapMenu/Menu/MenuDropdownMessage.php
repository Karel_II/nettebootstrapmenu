<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

use Nette\Utils\DateTime;

/**
 * Description of MenuDropdownMessages
 *
 * @author karel.novak
 * 
 * @property \Nette\Utils\DateTime $datetime Description 
 * @property string $class Description  
 */
class MenuDropdownMessage extends MenuObject implements Interfaces\IMenuItemSecurity, Interfaces\IMenuMetaRefresh, Interfaces\IMenuLink, Interfaces\IMenuHasParent, Interfaces\IMenuHasBranches {

    protected $datetime = NULL;
    protected $class = NULL;

    use Traits\TMenuItemSecurity;
    use Traits\TMenuMetaRefresh;
    use Traits\TMenuLink;
    use Traits\TMenuHasParent;
    use Traits\TMenuHasBranches;

    function getDatetime() {
        return $this->datetime;
    }

    function setDatetime($datetime) {
        $this->datetime = (isset($datetime)) ? $datetime : new DateTime();
    }

    /* Static Creators */

    public static function newMenuDropdownMessage($id, $parentId, $name, $description, $icon, $datetime = NULL, $class = NULL, $linkDestination = NULL, $linkArgs = array(), $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setLinkDestination($linkDestination);
        $return->setLinkArgs($linkArgs);
        $return->setParentId($parentId);
        $return->setDatetime($datetime);
        $return->setClass($class);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
