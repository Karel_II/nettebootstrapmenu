<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuCounter
 *
 * @author Karel_II
 * 
 * @property array $counters Description 
 */
class MenuCounter extends MenuObject implements Interfaces\IMenuItemSecurity, Interfaces\IMenuLink, Interfaces\IMenuHasParent {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuLink;
    use Traits\TMenuHasParent;

    protected $counters = array();

    /* Counters */

    function getCounters() {
        return $this->counters;
    }

    function setCounters($counters) {
        $this->counters = $counters;
    }

    /* Static Creators */

    public static function newMenuCounter($id, $parentId, $name, $description, $icon, $counters, $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setCounters($counters);
        $return->setParentId($parentId);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
