<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

use NetteBootstapMenu\Menu\IMenuItem;

/**
 *
 * @author karel.novak
 */
interface IMenuHasParent {

    public function getParentId();

    public function getParent();

    public function getParents();

    public function setParentId($parentId);

    public function setParent(IMenuItem $parent);
}
