<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

use Nette\Application\UI\Presenter;

/**
 *
 * @author karel.novak
 */
interface IMenuLink {

    public function getLinkDestination();

    public function getLinkArgs();

    public function setLinkDestination($linkDestination);

    public function setLinkArgs($linkArgs);

    public function comparePresenter(Presenter $presenter, $parameters = array());
}
