<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author karel.novak
 * 
 * alternate	Provides a link to an alternate representation of the document (i.e. print page, translated or mirror)
 * author	Provides a link to the author of the document
 * bookmark	Permanent URL used for bookmarking
 * external	Indicates that the referenced document is not part of the same site as the current document
 * help	        Provides a link to a help document
 * license	Provides a link to copyright information for the document
 * next	        Provides a link to the next document in the series
 * nofollow	Links to an unendorsed document, like a paid link. ("nofollow" is used by Google, to specify that the Google search spider should not follow that link)
 * noreferrer	Requires that the browser should not send an HTTP referer header if the user follows the hyperlink
 * noopener	Requires that any browsing context created by following the hyperlink must not have an opener browsing context
 * prev	        The previous document in a selection
 * search	Links to a search tool for the document
 * tag	        A tag (keyword) for the current document
 * 
 * 
 * Meta robots:
 * noindex – Zabraňuje indexování stránky.
 * nofollow: Zabrání Googlebotu ve sledování odkazů z této stránky.
 * nosnippet – Zabrání zobrazení textového úryvku nebo náhledu videa ve výsledcích vyhledávání. Pokud je to možné, bude u videa namísto náhledu zobrazen statický obrázek.
 * noarchive – Zabrání Googlu v zobrazování odkazu Archiv pro stránku.
 * unavailable_after:[datum] – Umožňuje určit přesný čas a datum, kdy se má stránka přestat procházet a indexovat.
 * noimageindex – Umožňuje určit, že nechcete, aby se stránka zobrazovala jako odkazovaná stránka pro obrázek ve výsledcích vyhledávání Google.
 * none – Je ekvivalentní k noindex, nofollow.
 */
interface IRobots {

    CONST A_REL_ALTERNATE = 'alternate',
            A_REL_AUTHOR = 'author',
            A_REL_BOOKMARK = 'bookmark',
            A_REL_EXTERNAL = 'external',
            A_REL_HELP = 'help  ',
            A_REL_LICENSE = 'license',
            A_REL_NEXT = 'next',
            A_REL_NOFOLLOW = 'nofollow',
            A_REL_NOREFERRER = 'noreferrer',
            A_REL_NOOPENER = 'noopener',
            A_REL_PREV = 'prev',
            A_REL_SEARCH = 'search',
            A_REL_TAG = 'tag';
    CONST META_ROBOTS_ALL = 'all',
            META_ROBOTS_NONE = 'none',
            META_ROBOTS_NOINDEX = 'noindex',
            META_ROBOTS_NOFOLOW = 'nofollow',
            META_ROBOTS_NOSNIPPET = 'nosnippet',
            META_ROBOTS_NOARCHIVE = 'noarchive',
            META_ROBOTS_UNAVAILABLE_AFTER = 'unavailable_after',
            META_ROBOTS_NOIMAGEINDEX = 'noimageindex';

    /**
     * 
     * @return string|null
     */
    public function getARel();

    /**
     * 
     * @return string|null
     */
    public function getMetaRobots();

    /**
     * 
     * @return boolean
     */
    public function isDisallow();

    /**
     * 
     * @param string $aRel
     * @return $this
     * @throws Exception
     */
    public function addARel($aRel);

    /**
     * 
     * @param string $metaRobots
     * @return $this
     * @throws Exception
     */
    public function addMetaRobots($metaRobots);

    /**
     * 
     * @param string|array $aRel
     * @return $this
     * @throws Exception
     */
    public function setARel($aRel);

    /**
     * 
     * @param string|array $metaRobots
     * @return $this
     * @throws Exception
     */
    public function setMetaRobots($metaRobots);
}
