<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

use NetteBootstapMenu\Menu\IMenuItem;

/**
 *
 * @author Karel
 */
interface IMenuHasBranches {

    public function getBranches();

    public function getSubTree(IMenuItem $parent = null);

    public function addBranch(IMenuItem $branch);
}
