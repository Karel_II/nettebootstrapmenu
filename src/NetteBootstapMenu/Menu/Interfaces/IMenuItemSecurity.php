<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

use \Nette\Security\IAuthorizator,
    \Nette\Security\User;

/**
 *
 * @author karel.novak
 */
interface IMenuItemSecurity {

    /**
     * Has a user effective access to the Resource?
     * If $resource is NULL, then the query applies to all resources.
     * @param string $resource
     * @param string $privilege
     * @return self
     */
    public function isAllowed($resource = IAuthorizator::ALLOW, $privilege = IAuthorizator::ALL);

    /**
     * Is this user authenticated?
     * @param bool $check
     * @return self
     */
    public function isLoggedIn($check = NULL);

    /**
     * 
     * @param User $user
     * @return boolean
     */
    public function checkSecurity(User $user);
}
