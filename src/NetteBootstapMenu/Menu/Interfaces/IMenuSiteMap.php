<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author karel.novak
 */
interface IMenuSiteMap {

    const SITEMAP_ALWAYS = 'always',
            SITEMAP_HOURLY = 'hourly',
            SITEMAP_DAILY = 'daily',
            SITEMAP_WEEKLY = 'weekly',
            SITEMAP_MONTHLY = 'monthly',
            SITEMAP_YEARLY = 'yearly',
            SITEMAP_NEVER = 'never';

    public function getSiteMapLastmod();

    public function getSiteMapChangefreq();

    public function getSiteMapPriority();

    public function getSiteMap();

    public function setSiteMap($siteMapPriority = NULL, $siteMapChangefreq = NULL, $siteMapLastmod = NULL);
}
