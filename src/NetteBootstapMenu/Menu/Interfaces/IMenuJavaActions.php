<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author Karel
 */
interface IMenuJavaActions {

    public function getDataset();

    public function getEvents();

    public function getJavaActionString();

    public function setJavaActions($dataset, $events);
}
