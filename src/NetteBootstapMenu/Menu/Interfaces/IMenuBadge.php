<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author Karel
 */
interface IMenuBadge {

    /**
     * 
     * @return string|null
     */
    public function getBadgeText();

    /**
     * 
     * @param string $badgeText
     * @return $this
     */
    public function setBadgeText($badgeText);
}
