<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author karel.novak
 */
interface IMenuMetaRefresh {

    public function getMetaRefresh();

    public function setMetaRefresh($metaRefresh);
}
