<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu\Interfaces;

/**
 *
 * @author Karel
 */
interface IMenuUrl {

    /**
     * 
     * @return \Nette\Http\Url
     */
    function getUrl();

    /**
     * 
     * @param \Nette\Http\Url $url
     */
    function setUrl(\Nette\Http\Url $url);
}
