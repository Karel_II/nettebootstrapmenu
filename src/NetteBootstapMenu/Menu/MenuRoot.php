<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuRoot
 *
 * @author karel.novak
 */
class MenuRoot extends MenuObject implements Interfaces\IRobots, Interfaces\IMenuItemSecurity, Interfaces\IMenuMetaRefresh, Interfaces\IMenuLink {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuMetaRefresh;
    use Traits\TMenuLink;
    use Traits\TMenuHasBranches;
    use Traits\TRobots;

    /* Static Creators */

    public static function newMenuRoot($id, $name, $description, $icon, $linkDestination, $linkArgs = array(), $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setLinkDestination($linkDestination);
        $return->setLinkArgs($linkArgs);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
