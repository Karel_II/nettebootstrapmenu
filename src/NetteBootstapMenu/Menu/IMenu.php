<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

use Nette\Application\UI\Presenter;

/**
 * Description of IMenu
 *
 * @author Karel
 */
interface IMenu extends \ArrayAccess, \Countable, \IteratorAggregate {

    /**
     * 
     * @param \Nette\Application\UI\Presenter $presenter
     * @return \NetteBootstapMenu\MenuItem Description
     */
    public function getMenuItem(Presenter $presenter);

    /**
     * @return array Description
     */
    public function getKeys();
}
