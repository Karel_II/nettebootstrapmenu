<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuSearch
 *
 * @author Karel_II
 */
class MenuSearch extends MenuObject implements Interfaces\IMenuItemSecurity, Interfaces\IMenuLink, Interfaces\IMenuHasParent {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuLink;
    use Traits\TMenuHasParent;


    /* Static Creators */

    public static function newMenuSearch($id, $parentId, $name, $description, $icon, $linkDestination, $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setLinkDestination($linkDestination);
        $return->setParentId($parentId);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
