<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Menu;

/**
 * Description of MenuLink
 *
 * @author Karel_II
 */
class MenuLink extends MenuObject implements Interfaces\IRobots, Interfaces\IMenuItemSecurity, Interfaces\IMenuMetaRefresh, Interfaces\IMenuLink, Interfaces\IMenuHasParent, Interfaces\IMenuHasBranches, Interfaces\IMenuBadge, Interfaces\IMenuSiteMap, Interfaces\IMenuJavaActions {

    use Traits\TMenuItemSecurity;
    use Traits\TMenuMetaRefresh;
    use Traits\TMenuLink;
    use Traits\TMenuHasParent;
    use Traits\TMenuHasBranches;
    use Traits\TMenuBadge;
    use Traits\TRobots;
    use Traits\TMenuSiteMap;
    use Traits\TMenuJavaActions;
    /* Static Creators */

    public static function newMenuLink($id, $parentId, $name, $description, $icon, $linkDestination, $linkArgs = array(), $showMenu = TRUE) {
        $return = new static;
        $return->setId($id);
        $return->setName($name);
        $return->setDescription($description);
        $return->setIcon($icon);
        $return->setLinkDestination($linkDestination);
        $return->setLinkArgs($linkArgs);
        $return->setParentId($parentId);
        $return->setShowMenu($showMenu);
        return $return;
    }

}
