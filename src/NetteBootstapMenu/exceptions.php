<?php

namespace NetteBootstapMenu;

class RootBranchHaveParentException extends \Exception {

    protected $message = 'Root branch can\'t have parent !';

    public function __construct($message) {
        parent::__construct($message);
    }

}
