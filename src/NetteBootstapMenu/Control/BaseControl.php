<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

/**
 * Description of BaseControl
 *
 * @author Karel_II
 */
abstract class BaseControl extends \Nette\Application\UI\Control {

    /**
     * @var string 
     */
    protected $view = 'default';
    /*
     * 
     * @var bool     
     */
    protected $resetTemplateFile = false;

    /**
     *
     * @var Nette/Localization/ITranslator
     */
    protected $translator = NULL;

    /**
     * Return new subcontrol
     * @return \static
     */
    final protected function createComponentStatic() {
        $static = new static;
        return $static;
    }

    /**
     * 
     * @param string $functionName __FUNCTION__
     */
    protected function setViewFromFunctionName($functionName) {
        $m = array();
        preg_match('#render(\w+)$#', $functionName, $m);
        if (isset($m[1]) && !empty($m[1])) {
            $view = lcfirst($m[1]);
        } else {
            $view = 'default';
        }

        if (strcmp($view, $this->view) !== 0) {
            $this->view = $view;
            $this->resetTemplateFile = true;
        }
    }

    /**
     * Get template file names.
     */
    public function getTemplateFile(string $functionName): string {
        preg_match('#(\w+)Control$#', get_class($this), $m);
        preg_match('#render(\w+)$#', $functionName, $n);
        $this->view = (isset($n[1]) && !empty($n[1])) ? lcfirst($n[1]) : 'default';
        if (isset($m[1])) {
            $control = $m[1];
            $dir = dirname(static::getReflection()->getFileName());
            $dir = is_dir("$dir/templates") ? $dir : dirname($dir);
            $files = [
                "$dir/templates/$control/$this->view.latte",
                "$dir/templates/$control.$this->view.latte",
            ];
            foreach ($files as $file) {
                if (is_file($file)) {
                    return $file;
                }
            }
            $file = preg_replace('#^.*([/\\\\].{1,70})\z#U', "\xE2\x80\xA6\$1", reset($files));
            $file = strtr($file, '/', DIRECTORY_SEPARATOR);
            throw new \Exception("Page not found. Missing template '$file'.");
        }
    }

    /**
     * 
     * @param Nette/Localization/ITranslator $translator
     */
    public function setTranslator($translator) {
        $this->translator = $translator;
    }

    /**
     * 
     * @param string $functionName
     * @return void
     */
    protected function renderControl(string $functionName): void {
        $this->template->setTranslator($this->translator);
        $this->template->render($this->getTemplateFile($functionName));
    }
}
