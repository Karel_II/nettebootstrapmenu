<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control\Traits;

use NetteBootstapMenu\Menu\IMenuItem;

/**
 *
 * @author Karel
 */
trait TClassRenderSelector {
    //protected $classes = array();

    /**
     * 
     * @param IMenuItem $class
     * @param type $renderer
     * @throws \Exception
     */
    final protected function addClassRenderer(IMenuItem $class, $renderer) {
        if (class_exists($class) && (new $class instanceof IMenuItem)) {
            $this->classes[$class] = $renderer;
        } else {
            throw new \Exception();
        }
    }

    /**
     * 
     * @param IMenuItem $class
     * @return type
     * @throws \Exception
     */
    final protected function getClassRenderer(IMenuItem $class) {
        if (class_exists(get_class($class)) && ($class instanceof IMenuItem)) {
            return $this->classes[get_class($class)];
        } else {
            throw new \Exception();
        }
    }

}
