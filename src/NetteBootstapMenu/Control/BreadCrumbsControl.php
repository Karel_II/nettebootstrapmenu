<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

use Nette\Application\UI\Presenter;
use NetteBootstapMenu\Menu\IMenu;

/**
 * Description of BreadCrumbs
 *
 * @author Karel_II
 */
class BreadCrumbsControl extends BaseControl {

    public function render(IMenu $menu, Presenter $presenter) {
        $curentMenuItem = $menu->getMenuItem($presenter);
        $presenterAction = $presenter->action;
        $presenterName = $presenter->name;
        if (isset($curentMenuItem)) {
            $breadCrumbsItems = $curentMenuItem->getParents();
            $breadCrumbsItems[] = $curentMenuItem;
        } else {
            $breadCrumbsItems = NULL;
        }

        $this->template->presenterAction = $presenterAction;
        $this->template->presenterName = $presenterName;

        $this->template->breadCrumbsItems = $breadCrumbsItems;
        $this->renderControl(__FUNCTION__);
    }
}
