<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

use NetteBootstapMenu\Control\BaseControl;
use NetteBootstapMenu\Menu\IMenu,
    NetteBootstapMenu\Menu\IMenuItem;

/**
 * Description of Menu
 *
 * @author Karel_II
 */
class MenuFooterControl extends BaseControl {

    use Traits\TClassRenderSelector;

    /**
     *
     * @var array 
     */
    protected $classes = array(
        'NetteBootstapMenu\Menu\MenuRoot' => 'item',
        'NetteBootstapMenu\Menu\MenuLink' => 'item',
        'NetteBootstapMenu\Menu\MenuUrl' => 'item',
        'NetteBootstapMenu\Menu\MenuCheckBox' => 'item',
        'NetteBootstapMenu\Menu\MenuSeparator' => 'separator',
    );

    public function render($menu, $name = NULL) {
        if (empty($menu[0])) {
            return;
        }
        $this->template->name = $name;
        $this->template->root = $menu[0];
        $this->renderControl(__FUNCTION__);
    }

    final public function renderSelector(IMenuItem $menuItem) {
        $this->template->controlName = $this->getClassRenderer($menuItem);
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    final public function renderItems(IMenu $menu, $level, $class = NULL) {
        $this->template->menu = $menu;
        $this->template->level = $level;
        $this->template->class = $class;
        switch ($level) {
            case 1:
            case 2:
                break;
            default:
                throw new \Exception;
        }
        $this->renderControl(__FUNCTION__);
    }

    public function renderItem(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderSeparator(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }
}
