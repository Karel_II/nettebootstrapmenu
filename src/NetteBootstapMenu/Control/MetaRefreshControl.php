<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

use Nette\Application\UI\Presenter;
use NetteBootstapMenu\Menu\IMenu;

/**
 * Description of BreadCrumbs
 *
 * @author Karel_II
 */
class MetaRefreshControl extends BaseControl {

    public function render(IMenu $menu, Presenter $presenter) {
        $curentMenuItem = $menu->getMenuItem($presenter);
        $this->template->metaRefresh = NULL;
        $this->template->metaRobots = NULL;
        if ($curentMenuItem instanceof \NetteBootstapMenu\Menu\Interfaces\IMenuMetaRefresh) {
            $this->template->metaRefresh = $curentMenuItem->getMetaRefresh();
        }
        if ($curentMenuItem instanceof \NetteBootstapMenu\Menu\Interfaces\IRobots) {
            $this->template->metaRobots = $curentMenuItem->getMetaRobots();
        }
        $this->renderControl(__FUNCTION__);
    }

}
