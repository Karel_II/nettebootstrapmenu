<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

use Nette\Application\UI;
use NetteBootstapMenu\Menu\IMenu,
    NetteBootstapMenu\Menu\IMenuItem;

/**
 * Description of MenuHeaderControl
 *
 * @author Karel
 */
class MenuHeaderControl extends BaseControl {

    use Traits\TClassRenderSelector;

    /**
     *
     * @var string 
     */
    private $collapseClass = NULL;

    /**
     *
     * @var array 
     */
    protected $classes = array(
        'NetteBootstapMenu\Menu\MenuRoot' => 'item',
        'NetteBootstapMenu\Menu\MenuLink' => 'item',
        'NetteBootstapMenu\Menu\MenuUrl' => 'item',
        'NetteBootstapMenu\Menu\MenuCheckBox' => 'item',
        'NetteBootstapMenu\Menu\MenuSeparator' => 'separator',
        'NetteBootstapMenu\Menu\MenuSearch' => 'search',
        'NetteBootstapMenu\Menu\MenuDropdownMessage' => 'dropdownMessage',
    );

    /**
     * 
     * @param string $collapseClass
     */
    public function setCollapseClass($collapseClass) {
        $this->collapseClass = $collapseClass;
    }

    function createComponentMenuHorizontalSearchForm() {
        $form = new UI\Form;
        $form->addText('search')->setRequired();
        $form->addHidden('destination');
        $form->addSubmit('submit');
        $form->onSuccess[] = [$this, 'menuHorizontalSearchFormSucceeded'];
        return $form;
    }

    public function menuHorizontalSearchFormSucceeded(UI\Form $form, $values) {
        $this->presenter->redirect($values['destination'], $values['search']);
    }

    public function render(IMenu $brandMenu = NULL, IMenu $leftMenu = NULL, IMenu $searchMenu = NULL, IMenu $rightMenu = NULL) {
        
        $this->template->collapseClass = $this->collapseClass;
        $this->template->brandMenu = (isset($brandMenu[0])) ? $brandMenu[0] : NULL;
        $this->template->leftMenu = (isset($leftMenu[0])) ? $leftMenu[0] : NULL;
        $this->template->searchMenu = (isset($searchMenu[0])) ? $searchMenu[0] : NULL;
        $this->template->rightMenu = (isset($rightMenu[0])) ? $rightMenu[0] : NULL;
        $this->renderControl(__FUNCTION__);
    }

    final public function renderSelector(IMenuItem $menuItem) {
        
        $this->template->controlName = $this->getClassRenderer($menuItem);
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    final public function renderItems(IMenu $menu, $level, $class = NULL) {
        
        $this->template->menu = $menu;
        $this->template->level = $level;
        $this->template->class = $class;
        switch ($level) {
            case 1:
            case 2:
            case 3:
                break;
            default:
                throw new \Exception;
        }
        $this->renderControl(__FUNCTION__);
    }

    public function renderItem(IMenuItem $menuItem) {
        
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderSearch(IMenuItem $menuItem) {
        
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderSeparator(IMenuItem $menuItem) {
        
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderDropdownMessage(IMenuItem $menuItem) {
        
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

}
