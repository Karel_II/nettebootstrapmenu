<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteBootstapMenu\Control;

use Nette\Application\UI;
use NetteBootstapMenu\Menu\IMenu,
    NetteBootstapMenu\Menu\IMenuItem;

/**
 * Description of Menu
 *
 * @author Karel_II
 */
class MenuControl extends BaseControl {

    use Traits\TClassRenderSelector;

    /**
     *
     * @var string 
     */
    private $collapseClass = NULL;

    /**
     *
     * @var array 
     */
    protected $classes = array(
        'NetteBootstapMenu\Menu\MenuRoot' => 'item',
        'NetteBootstapMenu\Menu\MenuLink' => 'item',
        'NetteBootstapMenu\Menu\MenuUrl' => 'item',
        'NetteBootstapMenu\Menu\MenuCheckBox' => 'item',
        'NetteBootstapMenu\Menu\MenuSeparator' => 'separator',
        'NetteBootstapMenu\Menu\MenuSearch' => 'search',
        'NetteBootstapMenu\Menu\MenuCounter' => 'counter'
    );

    /**
     * 
     * @param string $collapseClass
     */
    public function setCollapseClass($collapseClass) {
        $this->collapseClass = $collapseClass;
    }

    function createComponentMenuSearchForm() {
        $form = new UI\Form;
        $form->addText('search')->setRequired();
        $form->addHidden('destination');
        $form->addSubmit('submit');
        $form->onSuccess[] = [$this, 'menuSearchFormSucceeded'];
        return $form;
    }

    public function menuSearchFormSucceeded(UI\Form $form, $values) {
        $this->presenter->redirect($values['destination'], $values['search']);
    }

    public function render($menu, $name = NULL) {
        $this->template->collapseClass = $this->collapseClass;
        if (empty($menu[0])) {
            return;
        }
        $this->template->name = $name;
        $this->template->root = $menu[0];
        $this->renderControl(__FUNCTION__);
    }

    final public function renderSelector(IMenuItem $menuItem) {
        $this->template->controlName = $this->getClassRenderer($menuItem);
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    final public function renderItems(IMenu $menu, $level, $class = NULL) {
        $this->template->menu = $menu;
        $this->template->level = $level;
        $this->template->class = $class;
        switch ($level) {
            case 1:
                $this->template->levelClass = '';
                break;
            case 2:
                $this->template->levelClass = 'nav-second-level';
                break;
            case 3:
                $this->template->levelClass = 'nav-third-level';
                break;
            default:
                throw new \Exception;
        }
        $this->renderControl(__FUNCTION__);
    }

    public function renderItem(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderSearch(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderSeparator(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }

    public function renderCounter(IMenuItem $menuItem) {
        $this->template->menuItem = $menuItem;
        $this->renderControl(__FUNCTION__);
    }
}
